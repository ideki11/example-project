package com.io.alazhar.apis;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.io.alazhar.dtos.UserDTO;
import com.io.alazhar.models.Users;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/user")
public class UserController extends HibernateCRUDController<Users, UserDTO> {

}
