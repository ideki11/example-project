package com.io.alazhar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlazharApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlazharApplication.class, args);
	}

}
