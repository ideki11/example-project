package com.io.alazhar.models;
// Generated Sep 5, 2019 10:30:33 AM by Hibernate Tools 5.4.3.Final

/**
 * PasswordResets generated by hbm2java
 */
public class PasswordResets implements java.io.Serializable {

	private PasswordResetsId id;

	public PasswordResets() {
	}

	public PasswordResets(PasswordResetsId id) {
		this.id = id;
	}

	public PasswordResetsId getId() {
		return this.id;
	}

	public void setId(PasswordResetsId id) {
		this.id = id;
	}

}
